package it.lundstedt.erik;
import com.fazecast.jSerialComm.SerialPort;
import it.lundstedt.erik.menu.Menu;
import java.io.IOException;
import java.io.OutputStream;
public class Testing
{
public static void main(String[] args) throws IOException {
	String[] portList = new String[SerialPort.getCommPorts().length];
	for (int i = 0; i < SerialPort.getCommPorts().length;	i++)portList[i]=i+") "+SerialPort.getCommPorts()[i].toString();
	for (int i = 0; i < portList.length;					i++)System.out.println(portList[i]);
	Menu.drawItems(portList);
	SerialPort comPort = SerialPort.getCommPorts()[1];
	comPort.openPort();
	comPort.setComPortTimeouts(SerialPort.TIMEOUT_READ_SEMI_BLOCKING, 0, 0);
	OutputStream serial = comPort.getOutputStream();
	serial.write(new byte[]{0,10,10});
	serial.write(new byte[]{'\n'});
	serial.flush();
	serial.close();
	comPort.closePort();
}
}
