package it.lundstedt.erik;

import com.fazecast.jSerialComm.SerialPort;
import it.lundstedt.erik.menu.Menu;

import java.io.IOException;
import java.io.OutputStream;

public class Main
{

public static void main(String[] args) {

	try {
//	String[] ports=jssc.SerialPortList.getPortNames();
		String[] portList = new String[SerialPort.getCommPorts().length];
		
		for (int i = 0; i < SerialPort.getCommPorts().length; i++) {
			portList[i]=i+") "+ SerialPort.getCommPorts()[i].toString();
		}
	String[] portsHeadder={"select a port","starts at 0"};
		int port;
	//    for (int i = 0; i < ports.length; i++) {
		//    System.out.println(port);
		port = UsrConf.defaultPort;
		if (UsrConf.doAskForPort)
		{
			Menu portMenu=new Menu(portsHeadder,portList);
			portMenu.drawItems();
			port=Menu.getChoice();
		}
		SerialPort serialPort=SerialPort.getCommPorts()[port];
		serialPort.openPort();
		serialPort.setComPortTimeouts(SerialPort.TIMEOUT_READ_SEMI_BLOCKING, 0, 0);
		serialPort.setBaudRate(UsrConf.baud);
		
		OutputStream serial = serialPort.getOutputStream();
		System.out.println("connected to port "+serialPort.getDescriptivePortName());
		Thread.sleep(Settings.delay/2);

		UsrConf.testLoop(serial);
		serial.close();
		UsrConf.exit(serialPort,serial);
		
	} catch (InterruptedException | IOException e) {
		e.printStackTrace();
	}
	
}





}
