package it.lundstedt.erik;

import com.fazecast.jSerialComm.SerialPort;
import it.lundstedt.erik.menu.Menu;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Scanner;

public class UsrConf
{
//portSettings
public static boolean doAskForPort=false;
public static int defaultPort=1;
public static int baud=9600;
public static byte mode=0;

public static Scanner input=new Scanner(System.in);

public static int pixelCount=Settings.pixelCount;

public static byte[] red=RGB.red;
public static byte[] green=RGB.green;
public static byte[] blue=RGB.blue;
public static byte[] pink=RGB.pink;
public static byte[] yellow=RGB.yellow;
public static byte[] orange=RGB.orange;
public static byte[] off={0,0,0};

public static String format="red,green,blue";
public static String[] effectHeader ={"pick a mode"};
public static String[] effectList={"0) chase","1) fine"};
public static String[] colourNames={"0 exit","1 red","2 green","3 blue","4 off","5 pink","6 yellow","7 orange"};
public static byte[][] rgbColours ={            red ,   green ,   blue ,   off ,   pink ,   yellow ,   orange};

static int length=colourNames.length-1;
public static String[] ColourHead ={"pick one","accepts integers between 1 and "+length};




public static void testLoop(OutputStream serial) throws IOException, InterruptedException {
	boolean doExit=false;
	while (!doExit) {
		int effect;
		Menu effectMenu=new Menu(UsrConf.effectHeader,UsrConf.effectList);
		Menu colourMenu = new Menu(UsrConf.ColourHead, UsrConf.colourNames);
		int colour;
		effectMenu.drawMenu();
		effect=Menu.getChoice();
		switch (effect) {
			
			case 0:
				colourMenu.drawMenu();
				colour = Menu.getChoice();
				if (colour == 0) {
					doExit = true;
				} else {
					serial.write((byte) 0);
					serial.write(new byte[]{0, 60});
					serial.write(UsrConf.rgbColours[colour - 1]);
					serial.write(new byte[]{'\n'});
					serial.flush();
				}
				break;
			case 1:
				colourMenu.drawMenu();
				colour = Menu.getChoice();
				if (colour == 0) {
					doExit = true;
				} else {
					

					/*
					System.out.println("startpoint:");
					int startPoint=Menu.getChoice();
					System.out.println("endpoint");
					int endPoint=Menu.getChoice();
					*/
					System.out.println("startingPixel, endingPixel");
					
					input.useDelimiter(",|\\s|\n");
					int startPoint=input.nextInt();
					int endPoint= input.nextInt();
					serial.write((byte) 1);
					serial.write(new byte[]{(byte) startPoint,(byte)endPoint});
					serial.write(UsrConf.rgbColours[colour - 1]);
					serial.write(new byte[]{'\n'});
					serial.flush();
				}
			break;
		}
		
		
		Thread.sleep(10);
	}
	
	
}




public static void loop(OutputStream serial) throws IOException, InterruptedException {
	boolean doExit=false;
	while (!doExit) {
		Menu colourMenu = new Menu(UsrConf.ColourHead, UsrConf.colourNames);
		colourMenu.drawMenu();
		int colour = Menu.getChoice();
		if (colour==0)
		{
			doExit=true;
		}
		else {
			serial.write(UsrConf.rgbColours[colour - 1]);
			serial.write(new byte[]{'\n'});
			serial.flush();
		}
		Thread.sleep(10);
	}
	
	
}


public static void exit(SerialPort serial, OutputStream stream) throws InterruptedException, IOException {
	String[] message={"exiting ",".",".",".\n","closing serial port ",".",".",".\n","done!"};//8
	for (int i = 0; i < message.length ; i++) {
		System.out.print(message[i]);
		Thread.sleep(500);
		if (i==4)//if message is closing serial
		{
			Thread.sleep(500);
			if (serial.isOpen()) serial.closePort(); stream.flush();stream.close();/*close the serial port*/
		}
	}
	System.exit(1);
}




}
